import dotenv from 'dotenv';
import express from 'express';
import { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import cors from 'cors';
import Routes from './routes';

const app = express();

// Load the .env file
dotenv.config();

// Set the environment
const { NODE_ENV, PORT } = process.env;
app.set('env', NODE_ENV);
app.set('port', PORT);

// Cors
app.use(
  cors({
    origin: '*',
    credentials: true,
  })
);

// Middleware
app.use(logger('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes
app.use('/', Routes);

// Route not found handler
app.use((req: Request, res: Response, next: NextFunction) => {
  const err: any = new Error(`${req.method} ${req.url} Not Found`);
  err.status = 404;
  next(err);
});

// Error handler
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  res.status(err.status);
  res.json({
    error: {
      message: err.message,
    },
  });
});

const server = app.listen(app.get('port'));
server.on('listening', () => {
  console.log(
    `Express Server Port: ${app.get('port')} | Environment : ${app.get('env')}`
  );
});

export { app, server };
