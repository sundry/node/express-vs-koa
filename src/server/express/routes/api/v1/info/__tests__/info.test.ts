import { expect } from 'chai';
const axios = require('axios');

const API_URL = `http://localhost:3000`;

describe('express routes', () => {
  it('should GET /api/v1/info', async () => {
    const res = await axios(`${API_URL}/api/v1/info`);
    expect(res.status).to.equal(200);
  });
});
