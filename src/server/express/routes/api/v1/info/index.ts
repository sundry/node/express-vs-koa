import express from 'express';
import { Request, Response } from 'express';

const router = express.Router();

router.get('/', (req: Request, res: Response) => {
  res.json({
    env: process.env.NODE_ENV,
    hostname: require('os').hostname(),
    tag: process.env.DOCKER_TAG,
  });
});

export default router;
