import express from 'express';
import HealthCheck from './healthcheck';
import Info from './info';

const router = express.Router();

router.use('/healthcheck', HealthCheck);
router.use('/info', Info);

export default router;
