import dotenv from 'dotenv';
import Koa from 'koa';
import cors from '@koa/cors';
import Routes from './routes';

const app = new Koa();

// Load the .env file
dotenv.config();

// Cors
app.use(
  cors({
    origin: '*',
    credentials: true,
  })
);

// logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  console.log(`${ctx.method} ${ctx.status} ${ctx.url} - ${rt}`);
});

// x-response-time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// Routes
app.use(Routes.routes()).use(Routes.allowedMethods());

app.on('error', (err, ctx) => {
  console.error(err, ctx);
});

const server = app.listen(3001);
server.on('listening', () => {
  console.log(
    `Koa Server Port: ${3001} | Environment : ${process.env.NODE_ENV}`
  );
});

export { app, server };
