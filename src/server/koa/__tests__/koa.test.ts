import { expect } from 'chai';
const axios = require('axios');

const API_URL = `http://localhost:3001`;

describe('koa routes', () => {
  it('should GET 404 /', async () => {
    const res = await axios(API_URL, { validateStatus: false });
    expect(res.status).to.equal(404);
  });
});
