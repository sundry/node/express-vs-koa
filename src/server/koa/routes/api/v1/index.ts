import Router from 'koa-router';
import HealthCheck from './healthcheck';
import Info from './info';

const router = new Router();

router.use('/healthcheck', HealthCheck.routes(), HealthCheck.allowedMethods());
router.use('/info', Info.routes(), Info.allowedMethods());

export default router;
