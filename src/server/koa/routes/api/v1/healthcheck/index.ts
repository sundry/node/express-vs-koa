import Router from 'koa-router';

const router = new Router();

router.get('/', (ctx, next) => {
  ctx.status = 200;
});

router.get('/error', (ctx, next) => {
  ctx.status = 500;
  throw new Error('An error happened');
});

export default router;
