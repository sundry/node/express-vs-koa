import { expect } from 'chai';
const axios = require('axios');

const API_URL = `http://localhost:3001`;

describe('koa routes', () => {
  it('should GET /api/v1/healthcheck', async () => {
    const res = await axios(`${API_URL}/api/v1/healthcheck`);
    expect(res.status).to.equal(200);
  });
  it('should GET /api/v1/healthcheck/error', async () => {
    const res = await axios(`${API_URL}/api/v1/healthcheck/error`, {
      validateStatus: false,
    });
    expect(res.status).to.equal(500);
  });
});
