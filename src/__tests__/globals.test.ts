import * as express from '../server/express';
import * as koa from '../server/koa';

const startExpressServer = new Promise(resolve =>
  express.server.on('listening', resolve)
);
const startKoaServer = new Promise(resolve =>
  koa.server.on('listening', resolve)
);

describe('express routes', () => {
  before(async () => {
    await new Promise(resolve => resolve(startExpressServer));
    await new Promise(resolve => resolve(startKoaServer));
  });
  after(async () => {
    await express.server.close();
    await koa.server.close();
  });
});
